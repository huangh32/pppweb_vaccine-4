import datetime, json, bson
from django.shortcuts import render,  redirect
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from PhmWeb.common import DateUtils, RequestUtils, DictUtils, Utils
from PhmWeb.biz.sessionmanager import SessionManager
from PhmWeb.utils.dbconfig import sql_ec_db
from PhmWeb.utils.logger import Logger
from somos.ses.email_verify_ses import EmailVerifySes


@csrf_exempt
def index(request):
    action = RequestUtils.get_string(request, 'action')
    if action == 'sendVerifyEmail':
        return handle_send_verify_email(request)
    else:
        return handle_show_result(request)


def handle_show_result(request):
    page_dict = dict()
    smg = SessionManager(request)
    # patient_id = 1001713792
    clinic_id = RequestUtils.get_clinic_id_by_host(request=request)
    if smg.session.PatientID <= 10:
        page_dict['AptInfo'] = '<div style="color:red">Invalid request.</div>'
        return render(request, 'somos/history.html', page_dict)

    biz = MyVaccinationBiz(clinic_id, smg.session.PatientID)
    apt_list = biz.get_my_appointment_list()
    page_dict['MyEmail'] = Utils.de_identify_email(biz.patient_email)

    show_verify_require = not biz.is_email_verified and biz.has_multiple_patients
    if show_verify_require:
        vo_verify = EmailVerifySes().find_effective_token_link(clinic_id=clinic_id, patient_id=smg.session.PatientID)
        print(f'vo_verify={vo_verify}')
        if vo_verify:
            show_verify_require = False
            page_dict['ShowVerifySent'] = True
    page_dict['ShowVerifyRequire'] = show_verify_require

    apt_info = '' if len(apt_list) > 0 else '<tr><td colspan=20>No Data.</td></tr>'

    for apt in apt_list:
        apt_id = apt['AptID']
        apt_info += '<tr aptid={0}>'.format(apt_id)
        apt_info += '''<td><i class="fa fa-user" aria-hidden="true" pid={2}></i> 
                             {0}, {1}<div style="color:gray"># {2}</div></td>'''.format(apt.get('PatientLastName'),
                                                                             apt.get('PatientFirstName'), apt['PatientID'])
        app_dt = apt.get('BeginDateTime')
        if isinstance(app_dt, datetime.datetime) or isinstance(app_dt, datetime.date):
            apt_info += '<td>{0}<br/>{1}'.format(app_dt.strftime('%B %d %Y'), app_dt.strftime('%A %I:%M %p'))
            order_id = DictUtils.get_int_value(apt, 'OrderID')
            if order_id > 0:
                apt_info += "<br/><span style='background-color:green; color:white' order_id={0}>&nbsp; 2nd Dose &nbsp;</span>".format(order_id)
            else:
                my_next_apt_list = [x for x in apt_list if x['OrderID'] == apt_id]
                if len(my_next_apt_list) > 0:
                    apt_info += "<br/><span style='background-color:purple; color:white' order_id={0}>&nbsp; 1st Dose &nbsp;</span>"
            apt_info += '</td>'
        else:
            apt_info += '<td></td>'

        site_info = '<div>'
        site_info += '  <div><i class="fa fa-address-card-o"></i> &nbsp;{0}</div>'.format(apt.get('LocationName'))
        site_info += '  <div><i class="fa fa-fw fa-map-marker"></i> {0}, {1}, {2} - {3}</div>'.format(apt.get('Address'),apt.get('City'),apt.get('State'), apt.get('Zip'))
        # site_info += '  <div><i class="fa fa-fw fa-mobile"></i> {0}</div>'.format(apt.get('Phone'))
        site_info += '</div>'
        apt_info += '<td>{0}</td>'.format(site_info)

        # vaccination info
        apt_info += '<td>'
        mft_name = apt.get('MFTName')
        if mft_name:
            apt_info += '<div>{0}</div>'.format(mft_name)
            apt_info += '<div style="color:gray">{0}</div>'.format(apt.get('LotNumber'))
            apt_info += '<div style="color:gray">{0}</div>'.format(DateUtils.datetime_to_short_string(apt.get('AdministrationDate')))
        apt_info += '</td>'

        status_label = format_apt_status_label(apt['Status'])
        apt_info += '<td>{0}</td>'.format(status_label)

        apt_info += '</tr>'

    page_dict['AptInfo'] = apt_info
    return render(request, 'somos/history.html', page_dict)


def handle_send_verify_email(request):
    ip = request.META['REMOTE_ADDR'] if request.META else ''
    smg = SessionManager(request)
    clinic_id = RequestUtils.get_clinic_id_by_host(request=request)

    biz = MyVaccinationBiz(clinic_id, smg.session.PatientID)
    apt_list = biz.get_my_appointment_list()
    # do not send email in test
    # need_send = True if ip != '127.0.0.1' or biz.patient_email.find('mdland') > 0 else False
    need_send = True
    if need_send:
        print(f'handle_send_verify_email, patient_email={biz.patient_email},is_email_verified={biz.is_email_verified}, has_multiple_patients={biz.has_multiple_patients}')
        if not biz.is_email_verified and biz.has_multiple_patients:
            EmailVerifySes().send_verify_email(clinic_id=clinic_id, patient_id=smg.session.PatientID, email_addr=biz.patient_email)
    return HttpResponse('ok')


def format_apt_status_label(apt_status):
    apt_status_label = '{0}'.format(apt_status)
    if apt_status in [0, 6]:
        apt_status_label = "<span  class='label label-purple'>In Schedule</span>"
    elif apt_status == 2:
        apt_status_label = "<span  class='label label-blue'>Checked in</span>"
    elif apt_status == 3:
        apt_status_label = "<span  class='label label-danger'>Cancelled</span>"
    elif apt_status == 13:
        apt_status_label = "<span  class='label label-danger'><s>Order Cancelled</s></span>"
    elif apt_status == 10:
        apt_status_label = "<span  class='label label-danger'>Completed</span>"
    return apt_status_label


class MyVaccinationBiz:
    def __init__(self, clinic_id, patient_id):
        self._clinic_id = clinic_id
        self._patient_id = patient_id
        self._is_email_verified = False
        self._related_patient_id_list = None
        self._patient_email = None
        self._ec_db = sql_ec_db(as_dict=True)
        self.init_verified_info()

    @property
    def is_email_verified(self):
        return self._is_email_verified

    @property
    def has_multiple_patients(self):
        return self._related_patient_id_list and len(self._related_patient_id_list) > 1

    @property
    def patient_email(self):
        if self._patient_email is None:
            return ''
        return self._patient_email

    def init_verified_info(self):
        sql = f"""select vv.VerifyID, pat.PatientEMail, pat.PatientID from Patient pat  
                 left join somos_pat_EmailVerify vv on pat.PatientEMail=vv.Email and vv.ClinicID={self._clinic_id}
                    where pat.PatientID={self._patient_id} and len(pat.PatientEMail)>10"""

        pat_email_dict = self._ec_db.query_first_row_to_dict(sql)
        print(f'init_verified_info={sql}, pat_email_dict={pat_email_dict}')

        verify_id = DictUtils.get_int_value(pat_email_dict, 'VerifyID')
        self._patient_email = pat_email_dict['PatientEMail'] if 'PatientEMail' in pat_email_dict else ''
        self._patient_email = self._patient_email.replace("'", '') if self._patient_email else ''

        if verify_id > 0:
            self._is_email_verified = True

        if len(self._patient_email) > 10:
            sql_pat_by_email = "select PatientID from Patient where ClinicID={1} and PatientEMail = '{0}' ".format(self._patient_email, self._clinic_id)
            print(f'sql_pat_by_email={sql_pat_by_email}')

            pat_id_list = self._ec_db.fetch_sql_dict_list(sql_pat_by_email)
            pat_id_list = [str(x['PatientID']) for x in pat_id_list]
            self._related_patient_id_list = pat_id_list if len(pat_id_list) < 5 else pat_id_list[0:4]
        print(f'self._related_patient_id_list={self._related_patient_id_list}')

    def get_my_appointment_list(self):
        sql_apt = f""" select top 20 apt.ID AptID, apt.BeginDateTime, apt.LocationID, apt.PatientID, apt.CreateDate,apt.LocationID , apt.Status ,q.qid
                    , ol.Name as LocationName, ol.Address, ol.City, ol.State,  ol.Phone , ol.Zip
                    , RTRIM(pat.PatientFirstName) as PatientFirstName, RTRIM(pat.PatientLastName) as PatientLastName, pat.PatientDOB
                    , apt.VaccineID, pvac.MFTName, pvac.LotNumber , pvac.AdministrationDate, pvac.Site as BodySite, apt.OrderID
                  from dbo.Appointment apt  
                   inner join dbo.Ref_ClinicOfficeLocation ol on apt.LocationID = ol.OLID
                   inner join dbo.Patient pat on apt.PatientID=pat.PatientID
                   left join somos_CovidRegQuestionnaire q on apt.id=q.appointmentid
                   left join Vaccine_Data_PatientVaccine pvac on apt.VaccineID=pvac.VaccineID
                    """
        sql_where = f" where apt.clinicid={self._clinic_id} "

        if self._related_patient_id_list is None or len(self._related_patient_id_list) <= 1 or not self.is_email_verified:
            sql_where += f"  and apt.patientid={self._patient_id} "
        else:
            sql_where += " and apt.patientid in ({0})".format(','.join(self._related_patient_id_list))

        sql_orderby = "  order by pat.PatientID, apt.BeginDateTime desc"
        sql_select = f'{sql_apt} {sql_where} {sql_orderby}'
        Logger(logger_name='s7_results').getlog().info(sql_select)
        apt_list = self._ec_db.excute_select(sql_select)
        return apt_list
