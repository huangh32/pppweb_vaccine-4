import random, datetime, json
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from PhmWeb.common import RequestUtils, get_PHM_db
from PhmWeb.utils.dbconfig import sql_ec_db
from PhmWeb.biz.sessionmanager import SessionManager, save_http_session, UserSession
from somos.biz.community_service import CommunityService


def index(request):
    # request.session['PatientID'] = 9988
    page_dict = dict()
    if request.method == 'POST':
        msg, pat = validate_login(request)

        if isinstance(pat, int) and pat == 0:
            page_dict['ErrorMessage'] = msg
        else:
            smg = SessionManager(request)
            response = HttpResponseRedirect('/p/results/')
            return smg.response_cookie(response, pat)
    page_dict['UserName'] = RequestUtils.get_safe_sql_string(request, 'username')
    return render(request, 'somos/login.html', page_dict)


def validate_login(request):
    msg = ''
    username = RequestUtils.get_safe_sql_string(request, 'username')
    password = RequestUtils.get_safe_sql_string(request, 'password')
    clinic_id = RequestUtils.get_clinic_id_by_host(request=request)
    if len(username) == 0 or len(password) == 0 or username.find('@') == -1:
        return 'Invalid email or password!', 0
    sql_find_user = f""" select s.RowID, s.PatientID, s.Email from somos_pat_PortalAccount s
                            inner join patient p on s.PatientID=p.PatientID
                                where p.ClinicID={clinic_id} and s.Email = '{username}' and s.Password = '{password}' """
    ec_db = sql_ec_db(as_dict=True)
    pat_list = ec_db.excute_select(sql_find_user)
    if len(pat_list) == 0:
        return "Invalid email or password.", 0

    pat = pat_list[0]
    sql_login_update = """insert into somos_pat_Online(PortalID, LoginDate, LoginAccount, ClinicID) values({0}, GETDATE(), '{1}', {2}) ;
                        update somos_pat_PortalAccount set LastLoginDate=GETDATE(),LoginCount=ISNULL(LoginCount, 0) +1 where RowID={0}
                        """.format(pat['RowID'], username, clinic_id)
    ec_db.excute_update(sql_login_update)

    return msg, pat

