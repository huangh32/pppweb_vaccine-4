import threading, platform, os
from PhmWeb.utils.dbconfig import get_PHM_db, get_DW_db


class DbPhmRefDbconfig:

    _instance_lock = threading.Lock()

    def __init__(self):
        self.description = 'webid, clinic '
        self._webserver_dict = dict() # web_id, ip_pro
        self._clinic_dict = dict()
        self._computer_name = os.environ['COMPUTERNAME'] if 'COMPUTERNAME' in os.environ else ''
        self.init_web_server()
        self.init_clinic()

    def init_web_server(self):
        zw = get_PHM_db()
        webserver_list = list(zw['phm_ref_DbConfig'].find({}))
        for item in webserver_list:
            web_id = item['dbid']
            self._webserver_dict[web_id] = item

    def init_clinic(self):
        dw = get_DW_db()
        cli_list = dw['Clinic'].find({},{'ClinicID':1, 'WebID':1})
        for cli in cli_list:
            clinic_id = cli['ClinicID']
            self._clinic_dict[clinic_id] = cli

    @classmethod
    def instance(cls, *args, **kwargs):
        if not hasattr(DbPhmRefDbconfig, "_instance"):
            with DbPhmRefDbconfig._instance_lock:   #为了保证线程安全在内部加锁
                if not hasattr(DbPhmRefDbconfig, "_instance"):
                    DbPhmRefDbconfig._instance = DbPhmRefDbconfig(*args, **kwargs)
        return DbPhmRefDbconfig._instance

