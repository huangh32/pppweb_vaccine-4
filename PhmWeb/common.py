import datetime, json, types, re, calendar,os,openpyxl
import smtplib
import locale
import base64

from django.templatetags.static import static
from xml.dom import minidom
from bson.objectid import ObjectId
from pyDes import triple_des, CBC, PAD_PKCS5
from email.header import Header
from email.mime.text import MIMEText
from bs4 import BeautifulSoup
from PhmWeb.utils.dbconfig import get_DW_db, get_PHM_db
from PhmWeb.settings import BASE_DIR
from urllib import parse
import requests, json
from django.core.files.storage import default_storage
from io import BytesIO
from hashlib import md5
from PhmWeb.settings import PHMSettings
from PhmWeb.biz.environment_variables import EnvironmentVariables


class Utils(object):
    @staticmethod
    def to_numeric(s):
        if s is None or s == '':
            return 0

        if isinstance(s, float) or isinstance(s, int):
            return s

        if not isinstance(s, str):
            s = str(s)

        try:
            if s.find('.') !=-1:
                return float(s)
            else:
                return int(s)
        except ValueError:
            return 0
        return 0

    @staticmethod
    def is_number(s):
        if not s:
            return False
        try:
            float(s)
            return True
        except (TypeError, ValueError):
            pass
        try:
            import unicodedata
            unicodedata.numeric(s)
            return True
        except (TypeError, ValueError):
            pass
        return False

    @staticmethod
    def is_float(s):
        try:
            float(s)
            return True
        except:
            return False

    # base64 encode
    def base64_encode(inputstr):
        bytesString = inputstr.encode(encoding="utf-8")
        encodestr = base64.b64encode(bytesString)
        strpathinfo = str(encodestr, encoding="utf-8")
        return strpathinfo

    def base64_decode(encodestr):
        decodestr = base64.b64decode(encodestr)
        strres = str(decodestr, encoding="utf-8")
        return strres

    @staticmethod
    def is_empty_str(vl):
        if vl is None or vl == '':
            return True
        return False

    @staticmethod
    def is_not_empty(vl):
        if vl is None or vl == "":
            return False
        if isinstance(vl, str):
            vl = vl.strip()
            return len(vl) > 0
        else:
            vl = str(vl)
            return len(vl) > 0

    @staticmethod
    def parse_html_text(html):
        if html is None:
            return ''
        soup = BeautifulSoup(html, "html.parser")
        spans = soup.findAll('span')
        if spans:
            items = [i.text for i in spans]
            return ', '.join(items)
        return html

    @staticmethod
    def de_identify_email(ss):
        if ss is None:
            return ''
        pos = ss.find('@')
        if pos > 0:
            return ss[0:2] + '****' + ss[pos:]
        return ss


class DictUtils(object):
    @staticmethod
    def get(dictionary,key,d=None):
        if dictionary is None:
            return None
        return dictionary.get(key, d)

    @staticmethod
    def get_int_value(dictionary, name):
        val_str = DictUtils.get_value(dictionary, name)
        if val_str == '':
            return -1
        else:
            return int(val_str)

    @staticmethod
    def get_float_value(dictionary, name):
        val_str = DictUtils.get_value(dictionary, name)

        if val_str == '':
            return 0
        else:
            vl = Utils.to_numeric(val_str)
            # print('name={0},val_str={1}, vl={2}'.format(name,val_str, vl))
            return vl

    '''
    fmt = '({0})'
    '''

    @staticmethod
    def get_value(dictionary, name, fmt=None):
        if dictionary is None:
            return ''
        attr = name.split('.')[0] if '.' in name else name

        if attr in dictionary:
            val = dictionary[attr]
            if isinstance(val, datetime.datetime):
                return DateUtils.datetime_to_short_string(val)
            if attr != name:
                key = '.'.join(name.split('.')[1:])
                try:
                    if key in val:
                        if isinstance(val[key], datetime.datetime):
                            return DateUtils.datetime_to_long_string2(val[key])
                        return val[key] if fmt is None else fmt.format(val[key])
                    else:
                        if '.' in key:
                            return DictUtils.get_value(val, key, fmt)
                        else:
                            return ''
                except:
                    return ''
            # return val if val != '' and val is not None else ''
            if val is not None:
                return val if fmt is None else fmt.format(val)
        return ''

    @staticmethod
    def get_long_string_value(dictionary, name, same_timezone=False):
        if dictionary is None:
            return ''
        if name in dictionary:
            val = dictionary[name]
            if isinstance(val, datetime.datetime):
                return DateUtils.datetime_to_long_string(val, same_timezone)
            return val
        return ''

    @staticmethod
    def get_short_string_value(dictionary, name):
        if dictionary is None:
            return ''
        if name in dictionary:
            val = dictionary[name]
            if isinstance(val, datetime.datetime):
                return DateUtils.datetime_to_short_string(val)
            return val
        return ''

    @staticmethod
    def get_date_value(dic, name):
        if dic is None:
            return ''
        if name in dic:
            val = dic[name]
            if isinstance(val, datetime.datetime):
                return val
        return None


class DateUtils(object):

    @staticmethod
    # the default is return last year
    def get_past_year_date(past_years=1, past_months=0):
        dt_now = datetime.datetime.now()
        if past_months == 0:
            try:
                dt = dt_now.replace(year=dt_now.year - past_years)
                return dt
            except ValueError as e:
                dt = dt_now.replace(year=dt_now.year - past_years, day=dt_now.day - 1)
                return dt
        else:
            yyyy = dt_now.year - past_years - (past_months//12)
            mm = past_months % 12

            for i in range(3):
                dd = dt_now.day - i
                dt_str = f'{mm}/{dd}/{yyyy}'
                try:
                    dt = datetime.datetime.strptime(dt_str, "%m/%d/%Y")
                    return dt
                except ValueError as e:
                    continue

    # 把datetime转成字符串
    @staticmethod
    def datetime_to_short_string(dt):
        if dt is None:
            return ''
        if isinstance(dt, datetime.datetime) or isinstance(dt, datetime.date):
            return dt.strftime("%m/%d/%Y")
        if isinstance(dt, str):
            return dt
        return ''

    @staticmethod
    # iClinic Data minus 4 hours, for data generated by phmweb no need to change
    def datetime_to_long_string(dt, same_timezone=False, show_second=True):
        if dt is None:
            return ''
        if isinstance(dt, datetime.datetime) or isinstance(dt, datetime.date):
            nyc_dt = dt
            if not same_timezone:
                nyc_dt = dt + datetime.timedelta(hours=-4)
            if show_second:
                ss = nyc_dt.strftime("%m/%d/%Y %I:%M:%S %p")
            else:
                ss = nyc_dt.strftime("%m/%d/%Y %I:%M %p")
            return ss
        if isinstance(dt, str):
            return dt
        return ''

    @staticmethod
    def date_to_long_string(dt, same_timezone=False):
        if dt is None:
            return ''
        if isinstance(dt, datetime.datetime) or isinstance(dt, datetime.date):
            if not same_timezone:
                nyc_dt = dt + datetime.timedelta(hours=-4)
            else:
                nyc_dt = dt
            return nyc_dt.strftime("%Y-%m-%d %H:%M:%S")
        if isinstance(dt, str):
            return dt
        return ''

    @staticmethod
    def date_to_long_string3(dt):
        if dt is None:
            return ''
        if isinstance(dt, datetime.datetime) or isinstance(dt, datetime.date):
            nyc_dt = dt + datetime.timedelta(hours=-4)
            return nyc_dt.strftime("%m/%d/%Y %H:%M %p")
        if isinstance(dt, str):
            return dt
        return ''

    @staticmethod
    def datetime_to_long_string2(dt):
        if dt is None:
            return ''
        if isinstance(dt, datetime.datetime) or isinstance(dt, datetime.date):
            nyc_dt = dt + datetime.timedelta(hours=-4)
            str_dt = nyc_dt.strftime("%m/%d/%Y %I:%M %p")
            return str_dt.replace('12:00:00 AM', '')
        if isinstance(dt, str):
            return dt
        return ''

    # 把字符串转成datetime
    @staticmethod
    def string_to_datetime(string):
        if string is None:
            return -1
        if '/' in string:
            if string.index('/') == 4:
                return datetime.datetime.strptime(string, "%Y/%m/%d")
            return datetime.datetime.strptime(string, "%m/%d/%Y")
        if '-' in string:
            if string.index('-') == 4:
                return datetime.datetime.strptime(string, "%Y-%m-%d")
            return datetime.datetime.strptime(string, "%m-%d-%Y")
        if len(string) == 8:
            try:
                return datetime.datetime.strptime(string, "%Y%m%d")
            except:
                print(f'invalid for %Y%m%d {string}')

            try:
                return datetime.datetime.strptime(string, "%m%d%Y")
            except:
                print(f'invalid for %m%d%Y,{string}')
        return -1

    @staticmethod
    def string_long_to_datetime(string):
        return datetime.datetime.strptime(string, "%m/%d/%Y %I:%M:%S %p")


    def add_months(date, months):
        months_count = date.month + months
        # Calculate the year
        year = date.year + int(months_count / 12)

        # Calculate the month
        month = (months_count % 12)
        if month == 0:
            month = 12

        # Calculate the day
        day = date.day
        last_day_of_month = calendar.monthrange(year, month)[1]
        if day > last_day_of_month:
            day = last_day_of_month

        new_date = datetime.date(year, month, day)
        return new_date


    # 根据生日算年龄
    def calculate_age(born):
        if born is None:
            return ''
        today = datetime.datetime.now()

        if isinstance(born, str):
            born = DateUtils.string_to_datetime(born)

        if not isinstance(born, datetime.datetime) and not isinstance(born, datetime.date):
            return born

        try:
            birthday = born.replace(year=today.year)
        except ValueError:  # 2月29 闰年 无法转换
            # raised when birth date is February 29
            # and the current year is not a leap year
            try:
                birthday = born.replace(year=today.year, day=born.day - 1)
            except ValueError:
                return born
        if birthday > today:
            return today.year - born.year - 1
        else:
            return today.year - born.year


class EmailUtils(object):
    alert_to_list = ["aaa168@126.com"]  # ["940120528@qq.com"] #, "273980098@qq.com", "aaa168@126.com"
    @staticmethod
    def send_mail(subject, content):
        to_list = EmailUtils.alert_to_list
        # sub = "服务器告警"

        sender = 'pophealthny@163.com'
        password = "hrg12345678"  # web accout: hrg123456, smtp: hrg12345678

        msg = MIMEText(content, 'plain', 'utf-8')
        msg["Accept-Language"] = "zh-CN"
        msg["Accept-Charset"] = "ISO-8859-1,utf-8"
        msg['Subject'] = Header(subject, 'utf-8')
        msg['From'] = sender
        msg['To'] = ";".join(to_list)
        try:
            server = smtplib.SMTP()
            server.connect('smtp.163.com', '25')
            server.login(sender, password)
            server.sendmail(sender, to_list, msg.as_string())
            server.close()
            return True
        except Exception as e:
            print(str(e))
            print('send mail error... {0}'.format(str(e)))
            return False


class RequestUtils(object):

    @staticmethod
    def get_date(request, name):
        vl = get_param(request, name)
        vl = DateUtils.string_to_datetime(vl)
        if isinstance(vl, datetime.datetime):
            return vl
        return None

    @staticmethod
    def get_date_period(request, start_dos, end_dos):
        v_start = RequestUtils.get_date(request, start_dos)
        v_end = RequestUtils.get_date(request, end_dos)
        if v_start or v_end:
            res_dict = dict()
            if v_start:
                res_dict['$gte'] = v_start
            if v_end:
                res_dict['$lte'] = v_end + datetime.timedelta(minutes=24 * 60 -1)
            return res_dict
        return None

    @staticmethod
    def get_long(request, name, default=-1):
        vl = get_param_long(request, name)
        if default != -1 and vl in[-1, 0]:
            vl = default
        return vl

    @staticmethod
    def get_string(request, name, default=''):
        res_list = RequestUtils.get_list(request, name)
        if res_list:
            return ','.join(res_list)

        return get_param(request, name, default)

    @staticmethod
    def get_safe_sql_string(request, name):
        ss = RequestUtils.get_string(request, name)
        if ss is None:
            return ''
        ss = ss.replace("'", "''")
        return ss

    @staticmethod
    def get_list(request, name):
        res = request.GET.getlist(name, None)
        if not res:
            res = request.POST.getlist(name, None)
        return res

    @staticmethod
    def get_object_id(request, name):
        ss = RequestUtils.get_string(request, name)
        try:
            return ObjectId(ss)
        except:
            return None

    @staticmethod
    def get_action(request):
        return get_param(request, 'action')

    @staticmethod
    def get_clinic_list(request, param_name=None):
        if param_name is not None:
            ss = RequestUtils.get_string(request, param_name)
        else:
            ss = RequestUtils.get_string(request, 'clinic')
            if ss is None or len(ss)<3:
                ss = RequestUtils.get_string(request, 'clinicid')
        if ss is None or ss == '':
            return []
        ss_list = ss.strip().split(',')
        return [int(s) for s in ss_list]

    @staticmethod
    def get_clinic_id_by_host(request):
        if PHMSettings.FORCE_CLINIC_ID:
            return PHMSettings.FORCE_CLINIC_ID
        host = ("" + request.META['HTTP_HOST']) if 'HTTP_HOST' in request.META else ''
        if host and host.find('rend') != -1:
            return 3552
        # if the parameter has clinicid
        if request and len(request.GET.get('ClinicID', '')) == 4:
            return int(request.GET.get('ClinicID', ''))
        return 3553  # somos

    @staticmethod
    def fill_request_meta_log(request, log):
        if log is None:
            return
        log['VisitDate'] = datetime.datetime.now()

        log_meta = dict()
        values = request.META.items() if request.META else dict()
        for k, v in values:
            k = str(k)
            if k:
                k = k.replace('.', '_')
                log_meta[k] = str(v)
        log['META'] = log_meta
        log['PathInfo'] = request.META['PATH_INFO']

        if 'HTTP_X_FORWARDED_FOR' in request.META:
            log['IP'] = request.META['HTTP_X_FORWARDED_FOR']
        if 'REMOTE_ADDR' in request.META:
            log['RemoteAddr'] = request.META['REMOTE_ADDR']

    @staticmethod
    def get_request_remote_addr(request):
        if 'HTTP_X_FORWARDED_FOR' in request.META:
            return '' + request.META['HTTP_X_FORWARDED_FOR']
        if 'REMOTE_ADDR' in request.META:
            return '' + request.META['REMOTE_ADDR']


def request_get_str(request, key):
    if request.method == "GET":
        vl = request.GET.get(key, '')
        return vl
    if request.method == 'POST':
        vl = request.POST.get(key, '')
        return vl
    return ''


# DES加密
def encryption(data, key):
    if isinstance(key, str):
        key = key.encode(encoding='utf-8')
    while len(key) < 16:
        key += b'0'

    k = triple_des(key, CBC, b"\0\0\0\0\0\0\0\0", pad=None, padmode=PAD_PKCS5)
    d = k.encrypt(data)
    d = base64.b64encode(d)
    d = d.decode(encoding='utf-8')
    # print("Encrypted: %r" % d)

    return d


# 解密
def decrypt(cipher, key):
    # string 转 bytes
    key = key.encode(encoding='utf-8')

    # key如果不足16位在后面补0
    while len(key) < 16:
        key += b'0'
    cipher = cipher.replace(" ", "+")  # cipher作为url的一部分通过浏览器传递到服务端时，"+"会被替换为空格，此处需要还原回来
    cipher = base64.b64decode(cipher)

    k = triple_des(key, CBC, b"\0\0\0\0\0\0\0\0", pad=None, padmode=PAD_PKCS5)
    result = k.decrypt(cipher)

    # bytes 转 string
    result = result.decode(encoding='utf-8')
    print("Decrypted: %r" % result)
    return result


def get_gmt_time(iso_time):
    if iso_time:
        if isinstance(iso_time, datetime.datetime):
            return iso_time + datetime.timedelta(hours=-4)
        else:
            try:
                iso_time = DateUtils.string_long_to_datetime(iso_time)
                return iso_time + datetime.timedelta(hours=-4)
            except:
                return ''
    else:
        return datetime.datetime.now() + datetime.timedelta(hours=-4)


def get_gmt_value(dic, key):
    if key in dic:
        iso_time = dic[key]
        iso_time = get_gmt_time(iso_time)
        return DateUtils.datetime_to_long_string(iso_time)
    return ''


def get_param(request, name, default=''):
    res = request.GET.get(name, '')
    if not res:
        res = request.POST.get(name, '')
    if not res and default:
        res = default
    if res is not None and isinstance(res, str):
        res = res.strip()
    if res is None:
        return ''
    return res

def get_param_long(request, name):
    val_str = get_param(request, name)
    try:
        val_long = int(val_str)
        return val_long
    except:
        return -1


# ObejctID DateTime序列化
class ObjectEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, ObjectId):
            return str(obj)
        if isinstance(obj, datetime.datetime):
            return DateUtils.datetime_to_short_string(obj)
        if isinstance(obj, datetime.date):
            return DateUtils.datetime_to_short_string(obj)
        return json.JSONEncoder.default(self, obj)








