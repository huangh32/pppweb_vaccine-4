import random, datetime, re
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from PhmWeb.common import RequestUtils, get_PHM_db
from PhmWeb.utils.dbconfig import sql_ec_db
from PhmWeb.biz.aws_ses import AwsSES
from somos.ses.viph_welcome_ses import ViphealthWelcomeSes


def index(request):
    page_dict = dict()
    page_dict['email'] = RequestUtils.get_string(request, 'email')
    page_dict['mode'] = RequestUtils.get_string(request, 'mode')
    action = RequestUtils.get_string(request, 'action')
    if action == 'SubmitEmail':
        return submit_email(request)
    elif action == 'SaveAccountPwd':
        return save_account_pwd(request)
    return render(request, 'somos/recover.html', page_dict)


def submit_email(request):
    res_dict = {'ResultCode': 0, 'Message': ""}
    email = RequestUtils.get_safe_sql_string(request, 'Email')
    biz = RecoverAccount()
    clinic_id = RequestUtils.get_clinic_id_by_host(request=request)
    res_code = biz.send_access_code(email, clinic_id)
    if res_code > 0:
        res_dict = {'ResultCode': res_code, 'Message': "Failed"}
    return JsonResponse(res_dict)


def save_account_pwd(request):
    res_dict = dict({'ResultCode': 0, 'Message': "Good"})
    email = RequestUtils.get_safe_sql_string(request, 'Email')
    email = email.lower()
    access_code = RequestUtils.get_long(request, 'accessCode')
    password = RequestUtils.get_safe_sql_string(request, 'password')
    password2 = RequestUtils.get_safe_sql_string(request, 'password2')
    mode = RequestUtils.get_safe_sql_string(request, 'mode')
    result_code = 0
    message = ''

    # validate form data
    if password != password2:
        result_code = 1
        message = 'The password repeated does not match with the first.'
    elif len(password) < 8:
        result_code = 2
        message = 'The minimal length of password is 8.'

    if result_code > 0:
        res_dict = dict({'ResultCode': result_code, 'Message': message})
        return JsonResponse(res_dict)

    # validate access code
    zw = get_PHM_db()
    db_record = list(zw['vac_ses_AccessCode'].find({'PatientEMail': re.compile(email, re.IGNORECASE), 'AccessCode': access_code}).sort([('CreatedDate',-1)]))
    if len(db_record) == 0:
        print(f'PatientEMail={email}, AccessCode={access_code}')
        return JsonResponse({'ResultCode': 3, 'Message': 'Invalid access code'})
    else:
        zw['vac_ses_AccessCode'].update_one({'_id': db_record[0]['_id']}, {'$set': {'RegDate': datetime.datetime.now(),
                                                                                    'RegStatus': 1}})
    # save password
    pat_info = db_record[0]
    patient_id = pat_info['PatientID']
    email = pat_info['PatientEMail']
    if len(password) > 0:
        sql_account = f""" if not exists (select 1 from somos_pat_PortalAccount where PatientID={patient_id})
                            insert into somos_pat_PortalAccount(PatientID,Email, CreatedDate,UpdatedDate, Password)
                              values({patient_id}, '{email}', getDate(),getDate(), '{password}')
                         else 
                            update somos_pat_PortalAccount set Email='{email}', Password='{password}', UpdatedDate=getDate() where PatientID ={patient_id}
                         """
    else:
        sql_account = f""" if not exists (select 1 from somos_pat_PortalAccount where PatientID={patient_id})
                                insert into somos_pat_PortalAccount(PatientID,Email, CreatedDate,UpdatedDate, Password)
                                  values({patient_id}, '{email}', getDate(),getDate(), '{password}')
                             else 
                                update somos_pat_PortalAccount set Email='{email}', UpdatedDate=getDate() where PatientID ={patient_id}
                             """
    ec_db = sql_ec_db(as_dict=True)
    ec_db.excute_update(sql_account)

    # send welcome email
    if mode == "createAccount":
        sql = "select top 1 PatientFirstName from Patient where PatientID={0}".format(patient_id)
        pat = ec_db.query_first_row_to_dict(sql)
        patient_first_name = pat['PatientFirstName'].strip()
        url = request.scheme + "://" + request.META["HTTP_HOST"]
        #biz = RecoverAccount()
        #biz.email_welcome_message(patient_first_name,email, url)

    ec_db.close_conn()

    return JsonResponse(res_dict)


class RecoverAccount:
    def __init__(self):
        self._ec_db = sql_ec_db(as_dict=True)

    def send_access_code(self, email, clinic_id):
        if email == '' or email is None or email.find('@') == -1:
            return 1
        email = email.lower()
        sql_patient_email = "select top 10 PatientID, PatientMobilePhone, PatientEMail  from patient where clinicid={1} and PatientEMail='{0}'".format(email, clinic_id)
        pat_list = self._ec_db.excute_select(sql_patient_email)
        if len(pat_list) == 0:
            return 2
        pat = pat_list[0]

        # send code to email
        access_code = self.generate_access_code()
        self.email_access_code(access_code, pat['PatientEMail'])

        zw = get_PHM_db()
        pat['AccessCode'] = access_code
        pat['CreatedDate'] = datetime.datetime.now()
        zw['vac_ses_AccessCode'].insert_one(pat)
        return 0

    def generate_access_code(self):
        return random.randint(100000, 999999)

    def email_access_code(self, code, email):
        subject = 'Access code for account'
        content = 'Your access code is: {0}'.format(code)
        AwsSES().send_email(reciptent=email, subject=subject, content=content)

    def email_welcome_message(self, patient_first_name, email, url):
        return ViphealthWelcomeSes().email_welcome_message(patient_first_name, email, url)
