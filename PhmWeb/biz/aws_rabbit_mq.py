import pika
import json
import datetime
from bson import ObjectId
from PhmWeb.common import DateUtils
from PhmWeb.utils.dbconfig import get_PHM_db


class AwsRabbitMQ:
    def __init__(self):
        self.connection = self.get_connection()

    def get_connection(self):
        credentials = pika.PlainCredentials('mdland', 'egland20@899')  # mq account and password
        url = 'amqps://b-23ba9d65-b6d4-4f5f-9ab4-a3ad49fa67c3.mq.us-east-1.amazonaws.com:5671'
        params = pika.URLParameters(url)
        params.socket_timeout = 5
        params.credentials = credentials
        connection = pika.BlockingConnection(params) # Connect to CloudAMQP
        return connection

    def public_apt_sms(self, clinic_id, phone_number, sms_content, appointment_id=0, other_info=None):
        channel = self.connection.channel()
        # queue, if the quene not exists, it will be created
        quene = 'somos-appointment-sms'
        result = channel.queue_declare(queue=quene)
        mongodb_object_id = ObjectId()
        msg_dic = dict({'ClinicID': clinic_id, 'PhoneNumber': phone_number, 'SmsContent': sms_content, 'AppointmentID': appointment_id})
        msg_dic['ObjectID'] = str(mongodb_object_id)
        msg_dic['RequestDate'] = DateUtils.date_to_long_string(datetime.datetime.now())
        if other_info:
            for key in other_info:
                msg_dic[key] = other_info[key]
        message = json.dumps(msg_dic)
        if phone_number not in ['999-999-9999', '9999999999'] and len(phone_number) >= 8:
            channel.basic_publish(exchange='', routing_key=quene, body=message)

        # save to mongodg
        msg_dic['_id'] = mongodb_object_id
        get_PHM_db()['vac_log_AppointmentSMS'].insert_one(msg_dic)

    def close(self):
        self.connection.close()


#test case
if __name__ == '__main__':
    biz = AwsRabbitMQ()
    biz.public_apt_sms(1007, '917-8889-8888', sms_content='Hello AWS Eugene', appointment_id=8888)
    biz.close()

