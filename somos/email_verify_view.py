import datetime
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from PhmWeb.common import DateUtils, RequestUtils, DictUtils
from PhmWeb.utils.dbconfig import get_PHM_db
from PhmWeb.utils.dbconfig import sql_ec_db


def index(request):
    confirmation_token = RequestUtils.get_string(request, 'confirmation_token')
    if len(confirmation_token) != 40:
        return HttpResponse('Invalid confirmation token! ')
    zw = get_PHM_db()
    vo = zw['vac_ses_EmailVerity'].find_one({'Token': confirmation_token})
    if vo is None:
        return HttpResponse('Invalid confirmation token! ')

    generate_date = vo['GenerateDate']
    passed_ss = (datetime.datetime.now() - generate_date).total_seconds()
    if passed_ss > 3600:
        if vo['Status'] == 0:
            zw['vac_ses_EmailVerity'].update_one({'_id': vo['_id']}, {'$set': {'Status': 2,
                                                                               'ExpiredClickDate': datetime.datetime.now()}})
        return HttpResponse('Token expired!')
    if vo['Status'] == 1:
        return HttpResponse('Thanks, you have successfully confirmed your email..')

    if vo['Status'] == 0:
        # save a record db
        email = vo['Email']
        clinic_id = vo['ClinicID']
        patient_id = vo['PatientID']
        email = email.replace("'", "")
        sql_insert = f"""if not exists (select 1 from somos_pat_EmailVerify where Email='{email}' and ClinicID='{clinic_id}' ) 
                         insert into somos_pat_EmailVerify (Email, VerifiyDate,ClinicID, PatientID) values('{email}', GETDATE(), {clinic_id}, {patient_id});"""
        print(sql_insert)
        sql_ec_db().excute_update(sql_insert)
        zw['vac_ses_EmailVerity'].update_one({'_id': vo['_id']}, {'$set': {'Status': 1,
                                                                           'VerifiedClickDate': datetime.datetime.now()}})
        return HttpResponse('Thanks, you have successfully confirmed your email.')

    return HttpResponse(f'{passed_ss}')
