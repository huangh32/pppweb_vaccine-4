import random, datetime
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from PhmWeb.common import RequestUtils, get_PHM_db
from PhmWeb.utils.dbconfig import sql_ec_db
from PhmWeb.common import DateUtils, RequestUtils, get_PHM_db
from PhmWeb.biz.aws_ses import AwsSES
from PhmWeb.biz.sessionmanager import SessionManager
from PhmWeb.dbmodel.ic_patient import IcPatient
from PhmWeb.dbmodel.ic_covid_visitor import IcCovidVistor
from somos.biz.covid_booking_biz import CovidBookingBiz
from somos.biz.community_service import CommunityService
from somos.s3_register_view import PatientCrud
from somos.s_recover import RecoverAccount


def index(request):
    page_dict = dict()
    action = RequestUtils.get_string(request, 'action')
    smg = SessionManager(request)
    # fix data leak
    #patient_id = RequestUtils.get_long(request, 'patientid')
    #if patient_id <= 0:
    patient_id = smg.session.PatientID
    if action == 'SaveSignUp':
        return save_sign_up(request)
    page_dict['PageMode'] = 'SignUp'
    page_dict['PageButtonSubmit'] = 'Sign Up' if patient_id <= 0 else 'Update'
    page_dict['PageTitle'] = 'Sign Up' if patient_id <= 0 else 'Update My Profile'
    page_dict['PageShowPassword'] = True if patient_id <= 0 else False

    if patient_id > 0:
        pat_crud = PatientCrud(request)
        page_dict['Pat'] = pat_crud.get_patient_edit_info(patient_id)

    return render(request, 'somos/signup.html', page_dict)


def save_sign_up(request):
    res_dict = dict()
    error_msg = ''
    clinic_id = RequestUtils.get_clinic_id_by_host(request=request)
    email = RequestUtils.get_safe_sql_string(request, 'PatientEmail')
    visitBiz = IcCovidVistor()
    bookBiz = CovidBookingBiz()
    smg = SessionManager(request)

    patient_dob = RequestUtils.get_string(request, 'PatientDOB')
    if not CovidBookingBiz.is_valid_dob(patient_dob):
        res_dict['ResultType'] = 'Invalid DOB'
        res_dict['ResultCode'] = 1
        res_dict['Errors'] = "Your DOB not valid, please check."
        res_dict['HasError'] = True
        return JsonResponse(res_dict)

    visitor_id = bookBiz.find_existing_visitor(clinic_id, request)
    if visitor_id == 0:
        visitor_id = visitBiz.register_visitor(request, clinic_id)

    patient_id = smg.session.PatientID
    if patient_id > 0:
        ic_pat = IcPatient()
        update_mode = 'Update Profile'
        ic_pat.update_patient(request, patient_id=patient_id, clinic_id=clinic_id)
    else:
        patient_id = bookBiz.check_if_email_existed(clinic_id, email)
        update_mode = 'Sign Up'
        if patient_id > 0:
            # if patient existed, go to login
            error_msg = "This email account has been registered. Please log in or modify the email"
            res_dict['Errors'] = error_msg
            res_dict['HasError'] = len(error_msg) > 0
            response = JsonResponse(res_dict)
            # response = smg.response_cookie(response)
            return response

    if patient_id <= 0:
        patient_id = bookBiz.register_patient(clinic_id, visitor_id, request)

    # create email account, and login system
    email = RequestUtils.get_safe_sql_string(request, 'PatientEmail')
    password = RequestUtils.get_safe_sql_string(request, 'Password')
    if len(password) > 0 :
        sql_account = f""" if not exists (select 1 from somos_pat_PortalAccount where PatientID={patient_id})
                                insert into somos_pat_PortalAccount(PatientID,Email, CreatedDate,UpdatedDate, Password)
                                  values({patient_id}, '{email}', getDate(),getDate(), '{password}')
                             else 
                                update somos_pat_PortalAccount set Email='{email}', Password='{password}', UpdatedDate=getDate() where PatientID ={patient_id}
                             """
    else:
        sql_account = f""" if not exists (select 1 from somos_pat_PortalAccount where PatientID={patient_id})
                                        insert into somos_pat_PortalAccount(PatientID,Email, CreatedDate,UpdatedDate, Password)
                                          values({patient_id}, '{email}', getDate(),getDate(), '{password}')
                                     else 
                                        update somos_pat_PortalAccount set Email='{email}', UpdatedDate=getDate() where PatientID ={patient_id}
                                     """

    community_id = CommunityService(request).get_current_community()
    if community_id > 0:
        sql_account += """update Patient set CommunityID = {0} where PatientID={1}""".format(community_id, patient_id)

    ec_db = sql_ec_db(as_dict=True)
    ec_db.excute_update(sql_account)

    res_dict['Errors'] = error_msg
    res_dict['HasError'] = len(error_msg) > 0
    res_dict['GotoUrl'] = '/p/covid/' if update_mode == 'Sign Up' else '/p/results/'
    smg = SessionManager(request)
    response = JsonResponse(res_dict)
    pat = dict({'PatientID': int(patient_id)})
    response = smg.response_cookie(response, pat)

    if update_mode == 'Sign Up':
        patient_first_name = RequestUtils.get_safe_sql_string(request, 'PatientFirstName')
        url = request.scheme + "://" + request.META["HTTP_HOST"]
        biz = RecoverAccount()
        #biz.email_welcome_message(patient_first_name, email, url)

    return response
